package main

import (
	"bufio"
	"encoding/json"
	"html/template"
	"os"
	"path/filepath"
	"sort"
	"time"
)

type Metadata struct {
	Title  string `json:"title"`
	Author string `json:"author"`
	Slug   string `json:"slug"`
	Date   string `json:"date"`
}

type Post struct {
	Metadata
	Posted   time.Time
	Contents template.HTML
}

type PostHash map[string]*Post

type Posts []*Post

func (ps Posts) Len() int {
	return len(ps)
}

func (ps Posts) Less(i, j int) bool {
	return ps[i].Posted.After(ps[j].Posted)
}

func (ps Posts) Swap(i, j int) {
	ps[i], ps[j] = ps[j], ps[i]
}

var PostList Posts
var PostMap PostHash

func init() {
	var err error
	PostList, err = LoadPosts("posts/*.html")
	if err != nil {
		panic(err)
	}
	PostMap = BuildMap(PostList)
}

func LoadPosts(p string) (Posts, error) {
	fnames, err := filepath.Glob(p)
	if err != nil {
		return nil, err
	}
	posts := make(Posts, 0, len(fnames))
	for _, fname := range fnames {
		post, err := loadPost(fname)
		if err != nil {
			return nil, err
		}
		posts = append(posts, post)
	}
	sort.Sort(posts)
	return posts, nil
}

func loadPost(fname string) (*Post, error) {
	var post Post
	file, err := os.Open(fname)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	s := bufio.NewScanner(file)
	meta := make([]byte, 0, 128)
	var mdata Metadata
	for s.Scan() {
		if s.Text() == "" {
			break
		}
		meta = append(meta, s.Bytes()...)
	}
	if err := s.Err(); err != nil {
		return nil, err
	}
	err = json.Unmarshal(meta, &mdata)
	contents := make([]byte, 0, 1024)
	for s.Scan() {
		contents = append(contents, s.Bytes()...)
	}
	t, err := time.Parse(time.RFC3339, mdata.Date)
	if err != nil {
		return nil, err
	}
	post.Posted = t
	post.Metadata = mdata
	post.Contents = template.HTML(contents)
	return &post, err
}

func BuildMap(posts Posts) PostHash {
	postMap := PostHash{}
	for _, post := range posts {
		postMap[post.Slug] = post
	}
	return postMap
}
