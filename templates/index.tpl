{{ define "head" }}
<link rel="stylesheet" href="/static/lmr/stylesheet.css">
<link rel="stylesheet" href="/static/style.css">
<title>{{ .Title }}</title>
{{ end }}
{{ define "body" }}
<h1 class="masthead"><a href="/">Home</a></h1>
{{ range .Articles }}
<div class="article">
    <h1 class="article-title"><a href="/article/{{ .Slug }}">{{ .Title }}</a></h1>
    <h2 class="article-subtitle">{{ .Author }}&mdash;{{ .Posted }}</h2>
    <div class="contents">
	{{ .Contents }}
    </div>
</div>
{{ end }}
{{ end }}
