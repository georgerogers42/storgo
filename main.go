package main

import (
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	"html/template"
	"net/http"
)

func main() {
	on := flag.String("port", "8080", "The port to listen on")
	flag.Parse()
	http.Handle("/", AppRouter())
	http.Handle("/static/", http.FileServer(http.Dir("public")))
	fmt.Println("Listening on port:", *on)
	err := http.ListenAndServe(":"+*on, nil)
	if err != nil {
		panic(err)
	}
}

func AppRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/", IndexHandler)
	r.HandleFunc("/article/{slug:.*}", ArticleHandler)
	return r
}

type Articles struct {
	Title    string
	Articles Posts
}

type Article struct {
	Title   string
	Article *Post
}

var baseTpl = template.Must(template.ParseFiles("templates/base.tpl"))

var indexTpl = template.Must(template.Must(baseTpl.Clone()).ParseFiles("templates/index.tpl"))

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	indexTpl.Execute(w, Articles{Articles: PostList, Title: Title})
}

var articleTpl = template.Must(template.Must(baseTpl.Clone()).ParseFiles("templates/article.tpl"))

func ArticleHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	articleTpl.Execute(w, Article{Title: Title, Article: PostMap[vars["slug"]]})
}
